import { Component, OnInit, Input } from '@angular/core';
import { Ostrich } from 'src/app/classes/model/OstrichClass';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  @Input('ostrichs') ostrichs: Ostrich[];

  constructor() { }

  ngOnInit() {
  }

}
