import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { OstrichService } from 'src/app/services/ostrich.service';
import { Ostrich } from 'src/app/classes/model/OstrichClass';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  private _activeMenu: String = 'profile';

  setContent(menu: String){
    this._activeMenu = menu;
  }

  ostrichActivate(id: number){
    this.ostrichService.getById(id).subscribe(
      (res)=>{
        let ostrich = <Ostrich>res;
        ostrich.isActivated = true;
        console.log(ostrich);
        this.ostrichService.editOstrich(ostrich);
      }, (err)=>{
        console.log(err);
      }
    );
  }

  get userOstrichs(){
    return this.sessionService.userOstrichs;
  }

  get user(){
    return this.sessionService.user;
  }

  get userType(){
    return this.sessionService.userType;
  }

  get activeMenu(){
    return this._activeMenu;
  }

  constructor( 
    private sessionService:SessionService,
    private ostrichService: OstrichService ) { }

  ngOnInit() { 
    this.sessionService.load();
   }

}