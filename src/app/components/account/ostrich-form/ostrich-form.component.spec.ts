import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OstrichFormComponent } from './ostrich-form.component';

describe('OstrichFormComponent', () => {
  let component: OstrichFormComponent;
  let fixture: ComponentFixture<OstrichFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OstrichFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OstrichFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
