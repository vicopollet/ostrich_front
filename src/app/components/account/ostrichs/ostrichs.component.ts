import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ostrich } from 'src/app/classes/model/OstrichClass';

@Component({
  selector: 'app-ostrichs',
  templateUrl: './ostrichs.component.html',
  styleUrls: ['./ostrichs.component.css']
})
export class OstrichsComponent implements OnInit {

  @Input('ostrichs') ostrichs: Ostrich[];

  @Output('addOstrich') _add: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  add(){
    this._add.emit();
  }

  ngOnInit() {
  }

}
