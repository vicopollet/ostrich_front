import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OstrichsComponent } from './ostrichs.component';

describe('OstrichsComponent', () => {
  let component: OstrichsComponent;
  let fixture: ComponentFixture<OstrichsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OstrichsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OstrichsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
