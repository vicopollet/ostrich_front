import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ostrich } from 'src/app/classes/model/OstrichClass';

@Component({
  selector: 'app-announces',
  templateUrl: './announces.component.html',
  styleUrls: ['./announces.component.css']
})
export class AnnouncesComponent implements OnInit {

  @Input('ostrichs') ostrichs: Ostrich[];

  @Output('activate') _activate: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  activate(id:number){
    this._activate.emit(id);
  }

  ngOnInit() {
  }

}
