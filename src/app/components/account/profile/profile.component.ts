import { Component, OnInit, Input } from '@angular/core';
import { Person } from 'src/app/classes/model/PersonClass';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input('user') user: Person;

  constructor() { }

  ngOnInit() {
  }

}
