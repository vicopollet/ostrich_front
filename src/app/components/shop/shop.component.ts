import { Component, OnInit } from '@angular/core';
import { OstrichService } from 'src/app/services/ostrich.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  get ostrichs(){
    return this.ostrichService.ostrichs;
  }

  constructor(private ostrichService: OstrichService) { }

  ngOnInit() { }

}
