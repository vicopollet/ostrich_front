import { Component, OnInit } from '@angular/core';
import { OstrichService } from 'src/app/services/ostrich.service';
import { HashtagService } from 'src/app/services/hashtag.service';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  get hashtags(){
    return this.hashtagService.hashtags;
  }

  get ostrichs(){
    return this.ostrichService.ostrichs;
  }

  searchByCountry(country: String): void{
    if(!country){ 
      this.ostrichService.getAll(); 
      return; 
    }
    this.ostrichService.getByCountryName(country);
  }

  searchByHashtag(hashtagId: number): void{
    if(!hashtagId){
      this.ostrichService.getAll(); 
      return; 
    }
    this.ostrichService.getByHashtagId(hashtagId);
  }

  searchByPrice(min: number, max: number): void{
    if((max - min) <= 0){
      this.ostrichService.getAll(); 
      return; 
    }
    this.ostrichService.getByPriceRange(min, max);
  }

  reset(): void{
    this.ostrichService.getAll();
  }

  constructor(
    private ostrichService: OstrichService,
    private hashtagService: HashtagService  ) { }

  ngOnInit() {}

}
