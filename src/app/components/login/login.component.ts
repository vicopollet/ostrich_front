import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private errorMessage: String;

  formCheck(login: String, password: String){
    if(!login){ this.errorMessage = 'Please enter an email'; return; }
    if(!password){ this.errorMessage = 'Please enter a password'; return; }
    if(!login.match('[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,}')){
      this.errorMessage = 'Please enter a valid email';
      return;
    }
    this.errorMessage = '';
    this.login(login,password);
  }

  login(login: String, password: String){
    this.sessionService.login(login,password,()=>{ this.router.navigate(['/account']) })
  }

  constructor( 
    private sessionService:SessionService,
    private router: Router
    ) {  }

  ngOnInit() {
  }

}
