export enum OstrichRace {
    EMEU = "Emeu",
    CASOAR = "Casoar",
    NANDOU = "Nandou",
    AUTRUCHE = "Autruche",
    KIWI = "Kiwi"
}