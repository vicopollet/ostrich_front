export enum OrderStatus {
    PENDING ="Pending",
    COMPLETED = "Completed",
    SHIPPED = "Shipped",
    CANCELLED = "Cancelled",
    DECLINED = "Declined"
}