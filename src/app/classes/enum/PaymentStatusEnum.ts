export enum PaymentStatus {
    RECIEVED = "Recieved",
    AUTHORISED = "Authorised",
    DENIED = "Denied",
    CANCELLED = "Cancelled"
}