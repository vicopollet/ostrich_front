
export abstract class Person{

    private _id:number;
    private _firstName:String;
    private _lastName: String;
    private _birthDate: Date;
    private _email: String;
    private _password: String;
    private _phone: String;
    private _adressLine1: String;
    private _adressLine2: String;
    private _country: String;
    private _postCode: String;
    private _city: String;
    private _creationDate: Date;


    get id(): number {
		return this._id;
	}

    get firstName(): String {
		return this._firstName;
	}

    get lastName(): String {
		return this._lastName;
	}

    get birthDate(): Date {
		return this._birthDate;
	}

    get email(): String {
		return this._email;
	}

    get password(): String {
		return this._password;
	}

    get phone(): String {
		return this._phone;
	}

    get adressLine1(): String {
		return this._adressLine1;
	}

    get adressLine2(): String {
		return this._adressLine2;
	}

    get country(): String {
		return this._country;
	}

    get postCode(): String {
		return this._postCode;
	}

    get city(): String {
		return this._city;
	}

    get creationDate(): Date {
		return this._creationDate;
	}

    set id(value: number) {
		this._id = value;
	}

    set firstName(value: String) {
		this._firstName = value;
	}

    set lastName(value: String) {
		this._lastName = value;
	}

    set birthDate(value: Date) {
		this._birthDate = value;
	}

    set email(value: String) {
		this._email = value;
	}

    set password(value: String) {
		this._password = value;
	}

    set phone(value: String) {
		this._phone = value;
	}

    set adressLine1(value: String) {
		this._adressLine1 = value;
	}

    set adressLine2(value: String) {
		this._adressLine2 = value;
	}

    set country(value: String) {
		this._country = value;
	}

    set postCode(value: String) {
		this._postCode = value;
	}

    set city(value: String) {
		this._city = value;
	}

    set creationDate(value: Date) {
		this._creationDate = value;
	}

}