
import { OstrichRace } from '../enum/OstrichRaceEnum';
import { Cart } from './CartClass';
import { User } from './UserClass';
import { Hashtag } from './HashtagClass';
import { Farmer } from './FarmerClass';

export class Ostrich {

    private _id: number;
    private _name: String;
    private _description: String;
    private _country: String;
    private _size: number;
    private _weight: number;
    private _race: OstrichRace;
    private _price: number;
    private _hashtags: Hashtag[];
    private _farmer: Farmer;
    private _cart: Cart;
	private _favoutiteOf: User[];
	private _isActivated: boolean;

	get isActivated(): boolean{
		return this._isActivated;
	}

    get id(): number {
		return this._id;
	}

    get name(): String {
		return this._name;
	}

    get description(): String {
	    return this._description;
	}

    get country(): String {
		return this._country;
	}

    get size(): number {
		return this._size;
	}

    get weight(): number {
		return this._weight;
	}

    get race(): OstrichRace {
		return this._race;
	}

    get price(): number {
		return this._price;
	}

    get hashtags(): Hashtag[] {
		return this._hashtags;
	}

    get farmer(): Farmer {
		return this._farmer;
	}

    get cart(): Cart {
		return this._cart;
	}

    get favoutiteOf(): User[] {
		return this._favoutiteOf;
	}

	set isActivated(value: boolean){
		this._isActivated = value;
	}

    set id(value: number) {
		this._id = value;
	}

    set name(value: String) {
		this._name = value;
	}

    set description(value: String) {
		this._description = value;
	}

    set country(value: String) {
		this._country = value;
	}

    set size(value: number) {
		this._size = value;
	}

    set weight(value: number) {
		this._weight = value;
	}

    set race(value: OstrichRace) {
		this._race = value;
	}

    set price(value: number) {
		this._price = value;
	}

    set hashtags(value: Hashtag[]) {
		this._hashtags = value;
	}

    set farmer(value: Farmer) {
		this._farmer = value;
	}

    set cart(value: Cart) {
		this._cart = value;
	}

    set favoutiteOf(value: User[]) {
		this._favoutiteOf = value;
	}

	addHashtag(hashtag: Hashtag){
		this._hashtags.push(hashtag);
	}
	
	addFavoutiteOf(favourite: User){
		this._favoutiteOf.push(favourite);
	}
}
