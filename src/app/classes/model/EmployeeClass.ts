import { Person } from "./PersonClass";

export class Employee extends Person{

    private _employeeNumber: String;

	public get employeeNumber(): String {
		return this._employeeNumber;
	}

	public set employeeNumber(value: String) {
		this._employeeNumber = value;
	}

}