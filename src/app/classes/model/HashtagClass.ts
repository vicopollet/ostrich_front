import { Ostrich } from "./OstrichClass";

export class Hashtag{

    private _id: number;
    private _name: String;
    private _ostrichs: Ostrich[];

    get id(): number {
		return this._id;
	}

    get name(): String {
		return this._name;
	}

    get ostrichs(): Ostrich[] {
		return this._ostrichs;
	}

    set id(value: number) {
		this._id = value;
	}

    set name(value: String) {
		this._name = value;
	}

    set ostrichs(value: Ostrich[]) {
		this._ostrichs = value;
    }
    
    addOstrich(ostrich:Ostrich){
        this._ostrichs.push(ostrich);
    }

}