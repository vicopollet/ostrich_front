import { Ostrich } from "./OstrichClass";
import { Person } from "./PersonClass";

export class Farmer extends Person{

    private _ostrichs: Ostrich[];

    get ostrichs(): Ostrich[] {
		return this._ostrichs;
	}

    set ostrichs(value: Ostrich[]) {
		this._ostrichs = value;
    }
    
    addOstrich(ostrich: Ostrich){
        this._ostrichs.push(ostrich);
    }
}