import { Ostrich } from "./OstrichClass";
import { User } from "./UserClass";
import { Payment } from "./PaymentClass";
import { OrderStatus } from "../enum/OrderStatusEnum";

export class Cart{

    private _id: number;
    private _ostrichs: Ostrich[];
    private _user: User;
    private _payment: Payment;
    private _status: OrderStatus;
    private _creationDate: Date;


    get id(): number {
		return this._id;
	}

    get ostrichs(): Ostrich[] {
		return this._ostrichs;
	}

    get user(): User {
		return this._user;
	}

    get payment(): Payment {
		return this._payment;
	}

    get status(): OrderStatus {
		return this._status;
	}

    get creationDate(): Date {
		return this._creationDate;
	}

    set id(value: number) {
		this._id = value;
	}

    set ostrichs(value: Ostrich[]) {
		this._ostrichs = value;
	}

    set user(value: User) {
		this._user = value;
	}

    set payment(value: Payment) {
		this._payment = value;
	}

    set status(value: OrderStatus) {
		this._status = value;
	}

    set creationDate(value: Date) {
		this._creationDate = value;
    }
    
    addOstrich(ostrich: Ostrich){
        this._ostrichs.push(ostrich);
    }

}