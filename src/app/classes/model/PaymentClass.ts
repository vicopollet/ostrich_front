
import { PaymentStatus } from '../enum/PaymentStatusEnum';
import { Cart } from './CartClass';

export class Payment{
    private _id: number;
    private _totalPrice: number;
    private _status: PaymentStatus;
    private _cart: Cart;


    get id(): number {
		return this._id;
	}

    get totalPrice(): number {
		return this._totalPrice;
	}

    get status(): PaymentStatus {
		return this._status;
	}

    get cart(): Cart {
		return this._cart;
	}

    set id(value: number) {
		this._id = value;
	}

    set totalPrice(value: number) {
		this._totalPrice = value;
	}

    set status(value: PaymentStatus) {
		this._status = value;
	}

    set cart(value: Cart) {
		this._cart = value;
	}

}