
import { Person } from './PersonClass';
import { Ostrich } from './OstrichClass';
import { Cart } from './CartClass';

export class User extends Person{

    private _carts: Cart[];
    private _favourites: Ostrich[];
    
    get carts(): Cart[] {
		return this._carts;
	}

    get favourites(): Ostrich[] {
		return this._favourites;
	}

    set carts(value: Cart[]) {
		this._carts = value;
	}

    set favourites(value: Ostrich[]) {
		this._favourites = value;
    }
    
    addCart(cart: Cart){
        this._carts.push(cart);
    }

    addFavourite(favourite: Ostrich){
        this._favourites.push(favourite);
    }
}