import { Injectable } from '@angular/core';
//On importe httpclient ici et httpclientmodule dans le appmodule
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ostrich } from '../classes/model/OstrichClass';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OstrichService {

  //On importe la source dans le constructeur
  constructor( private http:HttpClient) { this.getAll() }

  private _ostrichs: Ostrich[];

  getAll(): void{
    this.http.get("http://localhost:8080/ostrichfarm/ostrich")
      .subscribe(
        (res)=>{
          this._ostrichs = <Ostrich[]>res;
        },(err)=>{
          console.log(err);
        }
      );
  }

  getByCountryName(country: String): void{
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/country/"+country)
      .subscribe(
        (res)=>{
          this._ostrichs = <Ostrich[]>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }

  getByHashtagId(hashtagId: number): void{
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/hashtag/"+hashtagId)
      .subscribe(
        (res)=>{
          this._ostrichs = <Ostrich[]>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }

  getByPriceRange(min: number, max: number): void{
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/price/"+ min + "/" + max)
    .subscribe(
      (res)=>{
        this._ostrichs = <Ostrich[]>res;
      }, (err)=>{
        console.log(err);
      }
    );
  }

  getById(id: number): Observable<any>{
    return this.http.get("http://localhost:8080/ostrichfarm/ostrich/" + id);
  }

  editOstrich(ostrich: Ostrich): void{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    this.http.put("http://localhost:8080/ostrichfarm/ostrich/" + ostrich.id, ostrich, {headers})
      .subscribe(
        (res)=>{
          console.log('edit success');
        }, (err)=>{
          console.log(err);
        }
      );
  }

  get ostrichs(): Ostrich[]{
    return this._ostrichs;
  }
}
