import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hashtag } from '../classes/model/HashtagClass';

@Injectable({
  providedIn: 'root'
})
export class HashtagService {

  constructor( private http:HttpClient) { this.getAll() }

  private _hashtags: Hashtag[];

  getAll(): void{
    this.http.get("http://localhost:8080/ostrichfarm/hashtag")
      .subscribe(
        (res)=>{
          this._hashtags = <Hashtag[]>res;
        },(err)=>{
          console.log(err);
        }
      );
  }

  get hashtags(): Hashtag[]{
    return this._hashtags;
  }

}
