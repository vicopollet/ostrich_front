import { TestBed } from '@angular/core/testing';

import { OstrichService } from './ostrich.service';

describe('OstrichService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OstrichService = TestBed.get(OstrichService);
    expect(service).toBeTruthy();
  });
});
