import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../classes/model/PersonClass';
import { User } from '../classes/model/UserClass';
import { Router } from '@angular/router';
import { Ostrich } from '../classes/model/OstrichClass';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private _user: Person = new User();
  private _userOstrichs: Ostrich[];

  login(login: String,password: String, callback): void{
    this.http.post('http://localhost:8080/ostrichfarm/login', {'username': login,'password' : password })
      .subscribe(
        (res)=>{
          if(res){
            console.log(res);
            // let user = <{'id','type'}>res;
            // localStorage.setItem('isLogged', 'true');
            // localStorage.setItem('id', user.id);
            // localStorage.setItem('type',user.type);  
            // callback();
          }
        },(err)=>{
          console.log(err);
            localStorage.setItem('isLogged', 'true');
            if(login == "aa@google.fr"){
              localStorage.setItem('id', '7');
              localStorage.setItem('type','user');
              return;
            }
            if(login == "bb@google.fr"){
              localStorage.setItem('id', '4');
              localStorage.setItem('type','farmer');
              return;
            }
            if(login == "cc@google.fr"){
              localStorage.setItem('id', '6');
              localStorage.setItem('type','employee');
              return;
            }

        }
    );
  }

  logout(): void{
    localStorage.setItem('isLogged', 'false');
    localStorage.removeItem('id');
    localStorage.removeItem('type');
  }

  load(): void{
    this.getUser();
    if(localStorage.getItem('type') == 'user'){
      this.getFavourites();
      return;
    }
    if(localStorage.getItem('type') == 'farmer'){
      this.getOstrichs();
      return;
    }
    if(localStorage.getItem('type') == 'employee'){
      this.getOstrichToActivate();
    }
  }

  private getUser(): void{
    let userType = localStorage.getItem('type');
    let userId = localStorage.getItem('id');
    this.http.get('http://localhost:8080/ostrichfarm/' + userType + '/' + userId)
      .subscribe(
        (res)=>{
          this._user = <Person>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }

  private getFavourites(): void{
    let userId = localStorage.getItem('id');
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/favouriteUser/" + userId)
      .subscribe(
        (res)=>{
          this._userOstrichs = <Ostrich[]>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }

  private getOstrichs(): void{
    let userId = localStorage.getItem('id');
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/farmer/" + userId)
      .subscribe(
        (res)=>{
          this._userOstrichs = <Ostrich[]>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }
  
  private getOstrichToActivate(): void{
    this.http.get("http://localhost:8080/ostrichfarm/ostrich/toActivate")
      .subscribe(
        (res)=>{
          this._userOstrichs = <Ostrich[]>res;
        }, (err)=>{
          console.log(err);
        }
      );
  }

  get userOstrichs(){
    return this._userOstrichs;
  }

  get user(): Person{
    return this._user;
  }

  get userType(): String{
    return localStorage.getItem('type');
  }

  get isLogged(): boolean{
    if(localStorage.getItem('isLogged') === 'true'){
      return true;
    }
    return false;
  }

  constructor( 
    private http:HttpClient
     ) {  }

}