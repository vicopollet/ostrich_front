import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})

export class LogginGuard implements CanActivate {

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (!this.sessionService.isLogged) {       
        this.router.navigate(['/login']);
        return false;     
      }     
      return true;   
  }
  
  constructor(    
    private router: Router,
    private sessionService: SessionService){}
}
