import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ShopComponent } from './components/shop/shop.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { FooterComponent } from './components/footer/footer.component';
import { FilterComponent } from './components/filter/filter.component';
import { AccountComponent } from './components/account/account.component';
import { CartComponent } from './components/account/cart/cart.component';
import { FavouritesComponent } from './components/account/favourites/favourites.component';
import { ProfileComponent } from './components/account/profile/profile.component';
import { OstrichsComponent } from './components/account/ostrichs/ostrichs.component';
import { ContactComponent } from './components/account/contact/contact.component';
import { AnnouncesComponent } from './components/account/announces/announces.component';
import { OstrichFormComponent } from './components/account/ostrich-form/ostrich-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShopComponent,
    HomeComponent,
    LoginComponent,
    SubscribeComponent,
    FooterComponent,
    FilterComponent,
    AccountComponent,
    CartComponent,
    FavouritesComponent,
    ProfileComponent,
    OstrichsComponent,
    ContactComponent,
    AnnouncesComponent,
    OstrichFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
